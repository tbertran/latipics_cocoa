//
//  LPLatitude.h
//  Latipics
//
//  Created by Thomas Bertran on 10/3/11.
//  Copyright 2011 self. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GTL/GTMOAuth2WindowController.h"
#import "GTLLatitude.h"
#import "LPPrefsPane.h"

@interface LPLatitude : NSObject{
    GTLLatitudeLocationFeed *locationsFeed_;
    GTLServiceTicket *locationsTicket_;
    NSError *locationsFetchError_;
    NSMutableArray * images;
    NSUInteger mAuthFetchersRunningCount;
    
}

@property (retain, nonatomic) GTLLatitudeLocationFeed *locationsFeed;
@property (retain, nonatomic) GTLServiceTicket *locationsTicket;
@property (retain, nonatomic) NSError *locationsFetchError;

- (GTLServiceLatitude *)latitudeService;
- (void) setImageswithImages:(NSMutableArray *)images;

@end
