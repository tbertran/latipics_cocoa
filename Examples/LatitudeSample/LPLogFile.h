//
//  LPLogFile.h
//  Latipics
//
//  Created by Thomas Bertran on 11/1/11.
//  Copyright (c) 2011 self. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

@interface LPLogFile : NSObject{

}

+ (void) createNewLogFile;
+ (void) setNoLogFile;
+ (BOOL) writeString:(NSString *) _string;
+ (NSString *) fileContents;
+ (NSArray *) logFileAsArray;
@end
