//
//  Image.h
//  Latipics
//
//  Created by Thomas Bertran on 10/3/11.
//  Copyright 2011 self. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GTLLatitude.h"
#import "LatitudeController.h"
#import "LPPrefsPane.h"

@interface LPImage : NSObject
{
    NSString * typeName;
    double latitude_;
    double longitude_;
    long long GLtimestampMs_;
    
    NSString *path_;
    NSURL *URL_;
    NSDate *imageDate;
    NSDate *fileCreatedOn_;
    BOOL hasBeenProcessed_;
    BOOL hasErrors_;    
    NSInteger returningRank_;
    BOOL hadInitiallyGPS_;
    NSMutableDictionary *tmpMetadata;
    CGImageRef					mImage;
    NSDictionary *				mMetadata;
    
}

@property (retain, nonatomic) NSString *path;
@property (retain, nonatomic) NSURL *URL;
@property (retain, nonatomic) NSDate *imageDate;
@property (retain, nonatomic) NSDate *fileCreatedOn;
@property (readwrite) NSInteger returningRank;
@property (readwrite) BOOL hasBeenProcessed;
@property (readwrite) BOOL hasErrors;
@property (readwrite) BOOL hadInitiallyGPS;
@property (readwrite) double latitude;
@property (readwrite) double longitude;
@property (readwrite) long long GLtimestampMs;

@property (readwrite) CGImageRef image;

- (LPImage *)initWithURL:(NSURL *)_URL;
- (BOOL) saveImage;
- (NSString*) pinName;
@end
