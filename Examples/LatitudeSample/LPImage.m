//
//  Image.m
//  Latipics
//
//  Created by Thomas Bertran on 10/3/11.
//  Copyright 2011 self. All rights reserved.
//

#import "LPImage.h"

NSDictionary * atts;

@implementation LPImage

@synthesize URL = URL_,
imageDate = imageDate_,
fileCreatedOn = fileCreatedOn_,
image = mImage,
hasBeenProcessed = hasBeenProcessed_,
returningRank = returningRank_,
latitude = latitude_,
longitude = longitude_,
GLtimestampMs = GLtimestampMs_,
hadInitiallyGPS = hadInitiallyGPS_,
hasErrors = hasErrors_,
path = path_;


- (NSString *)  imageRepresentationType
{
    return IKImageBrowserPathRepresentationType;
}


- (id)  imageRepresentation
{
    return [self path];
}

- (NSString *) imageUID
{
    return [self path];
}

- (NSString *) imageTitle
{
    return [[self URL] lastPathComponent];
}


- (NSString *) imageSubtitle{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd 'at' HH:mm"];
    return [dateFormatter stringFromDate:[self imageDate]];
}

- (NSString *) pinName{
    if ([self hasBeenProcessed]){
        if ([self hasErrors])
            return @"pin_red";
        else
            return @"pin_green";
    }
    
        return @"pin_gray";    
}

- (void)dealloc {
    [URL_ release];
    [path_ release];
    [imageDate release];
    [tmpMetadata release];
    [super dealloc];
}

+ (NSDate *)dateForTimestamp:(long)timestamp {
    
    // the server returns timestamp as a string, milliseconds since the unix epoch
    NSTimeInterval seconds = (NSTimeInterval)timestamp / 1000.0;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:seconds];
    return date;
}

- (NSDate *)dateForUnformattedDate:(NSString *) unformattedDate{

    
    NSDateFormatter* exifFormat = [[[NSDateFormatter alloc] init] autorelease];
    [exifFormat setDateFormat:@"yyyy:MM:dd HH:mm:ss"];
    NSDate* originalDate = [exifFormat dateFromString:unformattedDate];
    return originalDate; 
}
                                                                                                                    
- (BOOL) readImage
{
    
    NSDictionary* options = [[NSDictionary alloc] init];
    
    CGImageSourceRef source = CGImageSourceCreateWithURL((CFURLRef)[self URL], NULL);
    typeName = (NSString*)CGImageSourceGetType(source);
    mImage = CGImageSourceCreateImageAtIndex(source, 0, (CFDictionaryRef)options);
    mMetadata = (NSDictionary*)CGImageSourceCopyPropertiesAtIndex(source, 0, (CFDictionaryRef)options);
    if (!mMetadata){
        NSLog(@"Error. No metadata was found on %@. Skipped.", [self path]);
        [LPLogFile writeString:[NSString stringWithFormat:@"Error   : No metadata was found on %@. Skipped.", [[self URL]lastPathComponent]]];
        return NO;
    }    
    
    
    tmpMetadata = [[NSMutableDictionary dictionaryWithDictionary:mMetadata] retain];
    
    //Use the EXIFOriginalDate for "createdOn"
    NSDictionary *exif = [mMetadata objectForKey:(id) kCGImagePropertyExifDictionary];
    if (!exif){
        NSLog(@"Warning: No EXIF was found on picture %@. Created.", [self path]);
        [tmpMetadata setObject:(NSMutableDictionary *)[[NSMutableDictionary alloc] init] forKey:(id) kCGImagePropertyExifDictionary];
        imageDate_ = [self fileCreatedOn];
    }

    
    if (!(imageDate_ = [self dateForUnformattedDate:(NSString*) [exif objectForKey:(id)kCGImagePropertyExifDateTimeOriginal]]))
        imageDate_ = fileCreatedOn_;

    
    
    // Get GPS (if any)
    NSString* exifPath = [[NSBundle mainBundle] pathForResource:@"exiftool" ofType:@""];
    
    NSTask *task = [[NSTask alloc] init];
    [task setLaunchPath: exifPath];
    
    
    // Get Initial Latitude
    NSArray *arguments =    [NSArray arrayWithObjects:path_, 
                             @"-gps:GPSLatitude",    
                             @"-gps:GPSLongitude",    
                             @"-gps:GPSLatitudeRef",
                             @"-gps:GPSLongitudeRef",
                             @"-c",
                             @"%.6f",                             
                             nil];
    
    [task setArguments: arguments];
    
    NSPipe* pipe = [NSPipe pipe];
    [task setStandardOutput: pipe];
    
    NSFileHandle *file = [pipe fileHandleForReading];
    
    [task launch];
    
    NSData *data = [file readDataToEndOfFile];
    
    NSString *string = [[NSString alloc] initWithData: data
                                             encoding: NSUTF8StringEncoding];
    
    // String might not be what we think...
    NSRange _range = [string rangeOfString:@"GPS Latitude"];
    if (_range.location != NSNotFound){
        string = [string stringByReplacingOccurrencesOfString:@"\n" withString:@":"];
        NSArray *chunks = [string componentsSeparatedByString: @":"];
        
        
        if ([string length] != 0){
            [self setHadInitiallyGPS:YES];
            double _lat = [[chunks objectAtIndex:1] doubleValue];
            if ([[[chunks objectAtIndex:5] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@"South"])
                _lat = _lat * -1;
            [self setLatitude:_lat];
            
            double _long = [[chunks objectAtIndex:3] doubleValue];
            if ([[[chunks objectAtIndex:7] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@"West"])
                _long = _long * -1;
            [self setLongitude:_long];
            
        }
        else{
            [self setHadInitiallyGPS:NO];
        }
    }
    
    [task release];
    
    
    return YES;
}

- (BOOL) saveImage
{

    if (![LPPrefsPane overrideGPSInfo] && [self hadInitiallyGPS]){
        [LPLogFile writeString:[NSString stringWithFormat:@"Warning : %@ not tagged because GPS already present (user preference)", [[self URL] lastPathComponent]]];
        [self setHasErrors:YES];
        return NO;
    }
    
    
    //////////////////////////////////////
    // MAIN CALL /////////////////////////
    //////////////////////////////////////
    NSString* exifPath = [[NSBundle mainBundle] pathForResource:@"exiftool" ofType:@""];
    
    NSTask *task = [[NSTask alloc] init];
    [task setLaunchPath: exifPath];
    
    // Determine the references
    NSString *latRef, *longRef;
    double _latitude, _longitude;    
    
    if ([self longitude] < 0)
        longRef = @"W";
    else
        longRef = @"E";
    _longitude = fabs([self longitude]);
    
    
    if ([self latitude] < 0)
        latRef = @"S";
    else
        latRef = @"N";
    _latitude = fabs([self latitude]);
    
    NSDate *ts = [self.class dateForTimestamp: [self GLtimestampMs]];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm:ss.00"];
    
    
    
    NSMutableArray *tmpArguments;
    tmpArguments = [NSMutableArray arrayWithObjects:  [NSString stringWithFormat:@"-exif:GPSLatitudeRef=%@", latRef], 
                                            [NSString stringWithFormat:@"-exif:GPSLatitude=%@", [NSString stringWithFormat:@"%F", _latitude]], 
                                            [NSString stringWithFormat:@"-exif:GPSLongitudeRef=%@", longRef], 
                                            [NSString stringWithFormat:@"-exif:GPSLongitude=%@", [NSString stringWithFormat:@"%F", _longitude]],
                                            [NSString stringWithFormat:@"-exif:GPSTimeStamp=\"%@\"", [formatter stringFromDate:ts]],                 
                                            path_, 
                                            nil];
 
    // User prefs set to override updatedate
    if (![LPPrefsPane keepCopyOriginal])
        [tmpArguments insertObject:@"-overwrite_original" atIndex:0];
    
    // User prefs set to override updatedate
    if (![LPPrefsPane overrideUpdateDate])
        [tmpArguments insertObject:@"-P" atIndex:0];
    
        
    task = [[NSTask alloc] init];
    [task setLaunchPath: exifPath];
    
    NSArray *arguments = [NSArray arrayWithArray:tmpArguments];
    
    // Launch the task
    [task setArguments: arguments];    
    [task launch];
    
    // Mark image as processed
    [self setHasBeenProcessed:YES];
    return YES;    
    // ----------------------
    
    
    
    
    arguments = [NSArray arrayWithArray:tmpArguments];
    NSLog(@"\nSetting following GPS tag....");
    NSLog(@"%@\n",arguments);
    
    
    [task setArguments: arguments];
    [task setLaunchPath: exifPath];
    
    NSPipe *pipe = [NSPipe pipe];
    [task setStandardOutput: pipe];
    
    
    [task launch];
    
    
    // Mark image as processed
    [self setHasBeenProcessed:YES];
    return YES;
    
}

+ (NSArray *)readableTypesForPasteboard:(NSPasteboard *)pasteboard{
    return (NSArray*)CGImageSourceCopyTypeIdentifiers();
}


- (LPImage *)initWithURL:(NSURL *)__URL{
    self = [super init];
    
    URL_ = [__URL retain];
    [self setHasBeenProcessed:NO];
    path_ = [[URL_ path] retain];
    
    NSDictionary *dict = [[NSFileManager defaultManager] attributesOfItemAtPath:path_ error:nil];
    fileCreatedOn_ = [dict objectForKey:NSFileModificationDate];
    
    atts = [[[NSFileManager defaultManager] 
             attributesOfItemAtPath:[[self URL] path] 
             error:NULL] retain];
    
    if ([self readImage])
        return self;
    
    return nil;
}

@end
