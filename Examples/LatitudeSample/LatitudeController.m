/* Copyright (c) 2011 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//
//  LatitudeSampleWindowController.m
//

#import "LatitudeController.h"
#import "LPImage.h"
#import "LPLatitude.h"
#import "GTL/GTMOAuth2WindowController.h"
#import "GTL/GTMHTTPFetcherLogging.h"
#import "ImageBrowserBackgroundLayer.h"

@interface LatitudeController ()

//- (void)updateUI;
- (void)runSigninThenInvokeSelector:(SEL)signInDoneSel;
@end


NSString *const kKeychainItemName = @"Latipics";

@implementation LatitudeController


static NSString* const _CLIENT_ID = @"687895539659.apps.googleusercontent.com";
static NSString* const _CLIENT_SECRET = @"D3QWeF68vtPdSU319h7Bl9E2";
static NSString* const _KEYCHAIN_ITEM_NAME = @"Latipics";
LPLatitude * myLatitude;
static BOOL isRunning_;
NSTimer *timer;
NSTimer *mapTimer = nil;


+ (LatitudeController *)sharedWindowController {
    static LatitudeController* gWindowController = nil;
    if (!gWindowController) {
        gWindowController = [[LatitudeController alloc] init];
    }
    return gWindowController;
}


- (id)init {
    return [self initWithWindowNibName:@"Latipics"];
}

- (void)awakeFromNib {
    
    // Images supported by "Image I/O"
//    CFArrayRef mySourceTypes = CGImageSourceCopyTypeIdentifiers();
//    CFShow(mySourceTypes);
//    CFArrayRef myDestinationTypes = CGImageDestinationCopyTypeIdentifiers();
//    CFShow(myDestinationTypes);
    
    images = [[NSMutableArray alloc] init];
    importedImages = [[NSMutableArray alloc] init];
    
    // Allow reordering, animations and set the dragging destination delegate.
    [imageBrowser setAllowsReordering:NO];
    [imageBrowser setAllowsEmptySelection:NO];
    [imageBrowser setAnimates:YES];
    [imageBrowser setDraggingDestinationDelegate:self];
    [imageBrowser setCanControlQuickLookPanel:YES];
    [imageBrowser setAllowsMultipleSelection:YES];
    
    // customize the appearance
	[imageBrowser setCellsStyleMask:IKCellsStyleTitled | IKCellsStyleOutlined | IKCellsStyleSubtitled];
    
    // background layer
	ImageBrowserBackgroundLayer *backgroundLayer = [[[ImageBrowserBackgroundLayer alloc] init] autorelease];
	[imageBrowser setBackgroundLayer:backgroundLayer];
	backgroundLayer.owner = imageBrowser;
    
    
    //-- change default font 
	// create a centered paragraph style
	NSMutableParagraphStyle *paraphStyle = [[[NSMutableParagraphStyle alloc] init] autorelease];
	[paraphStyle setLineBreakMode:NSLineBreakByTruncatingTail];
	[paraphStyle setAlignment:NSCenterTextAlignment];
	
	NSMutableDictionary *attributes = [[[NSMutableDictionary alloc] init] autorelease];	
	[attributes setObject:[NSFont systemFontOfSize:10] forKey:NSFontAttributeName]; 
	[attributes setObject:paraphStyle forKey:NSParagraphStyleAttributeName];	 
	[attributes setObject:[NSColor blackColor] forKey:NSForegroundColorAttributeName];
	[imageBrowser setValue:attributes forKey:IKImageBrowserCellsTitleAttributesKey];
	
	attributes = [[[NSMutableDictionary alloc] init] autorelease];	
	[attributes setObject:[NSFont boldSystemFontOfSize:10] forKey:NSFontAttributeName]; 
	[attributes setObject:paraphStyle forKey:NSParagraphStyleAttributeName];	
	[attributes setObject:[NSColor blackColor] forKey:NSForegroundColorAttributeName];
	
	[imageBrowser setValue:attributes forKey:IKImageBrowserCellsHighlightedTitleAttributesKey];	
	
	//change intercell spacing
	[imageBrowser setIntercellSpacing:NSMakeSize(10, 60)];
	
	//change selection color
	[imageBrowser setValue:[NSColor yellowColor] forKey:IKImageBrowserSelectionColorKey];	
    
	//set initial zoom value
	[imageBrowser setZoomValue:0.5];
    
    
    
    // Authentication
    GTMOAuth2Authentication *auth;
    auth = [GTMOAuth2WindowController authForGoogleFromKeychainForName:_KEYCHAIN_ITEM_NAME
                                                              clientID:_CLIENT_ID
                                                          clientSecret:_CLIENT_SECRET];
    
    
    myLatitude = [[[LPLatitude alloc] init] retain];
    [[myLatitude latitudeService] setAuthorizer:auth];
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self
           selector:@selector(signInFetchStateChanged:)
               name:kGTMOAuth2FetchStarted
             object:nil];
    [nc addObserver:self
           selector:@selector(signInFetchStateChanged:)
               name:kGTMOAuth2FetchStopped
             object:nil];
    [nc addObserver:self
           selector:@selector(signInNetworkLost:)
               name:kGTMOAuth2NetworkLost
             object:nil];
    
    [self updateUI];
    
    // Set Log Drawer size
    NSSize mySize;
    mySize.width = 250;
    [logDrawer setContentSize:mySize];
    
    // Set Map Drawer size
    mySize.width = 350;
    [mapDrawer setContentSize:mySize];
    
    //set ourselves as the frame load delegate so we know when the window loads
    [mapWebView setFrameLoadDelegate:self];
    
    
    //turn off scrollbars in the frame
    [[[mapWebView mainFrame] frameView] setAllowsScrolling:NO];
    
    [logTableView setSelectionHighlightStyle:NSTableViewSelectionHighlightStyleNone];
    
}

- (void) dealloc
{
    [images release];
    [importedImages release];
    [super dealloc];
}

#pragma mark -
#pragma mark UI Methods
// -----------------------------------------------------------------
// UI METHODS
// -----------------------------------------------------------------
- (void) updateDatasource
{
    [images addObjectsFromArray:importedImages];
    [importedImages removeAllObjects];
    [imageBrowser reloadData];
}


#pragma mark -
#pragma mark IKImageBrowserDataSource

// Implement the image browser  data source protocol .
// The data source representation is a simple mutable array.

// -------------------------------------------------------------------------
//	numberOfItemsInImageBrowser:view
// ------------------------------------------------------------------------- 
- (NSUInteger)numberOfItemsInImageBrowser:(IKImageBrowserView*)view
{
	// The item count to display is the datadsource item count.
    return [images count];
}

// -------------------------------------------------------------------------
//	imageBrowser:view:index:
// ------------------------------------------------------------------------- 
- (id)imageBrowser:(IKImageBrowserView *) view itemAtIndex:(NSUInteger) index
{
    return [images objectAtIndex:index];
}

- (void) loadMapWebView{
    if ([images count] > 0){ // && [mapDrawer state] == NSDrawerOpenState){
        LPImage *selectedImage = [images objectAtIndex:[[imageBrowser selectionIndexes] firstIndex]];
        
        if ([selectedImage latitude] != 0 && [selectedImage longitude] != 0)
        {
            NSSize drawerSize = [mapDrawer contentSize];    
            NSString *latLong = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/staticmap?center=%f,%f&zoom=13&size=%dx%d&sensor=false&markers=color:purple%%7C%f,%f"
                                 ,[selectedImage latitude],[selectedImage longitude],(int)drawerSize.width, (int)drawerSize.height, [selectedImage latitude],[selectedImage longitude]];
            
            NSLog(@"URL: %@",latLong);
            [[mapWebView mainFrame] loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:latLong]]];
        }
        else{
            if ([selectedImage hasBeenProcessed]){
                NSString* htmlCode = @"<html><body bgcolor=\"#EDEDED\"><div style=\"font:16px helvetica,sans-serif; position: absolute; left: 10%;right: 10%; top: 30%;vertical-align: middle;\"><span style=\"font-weight:bold\">No GPS information could be found for this picture.</span><br><br>You could try to adjust the time range settings in the preferences.</div></body></html>" ;
                [[mapWebView mainFrame] loadHTMLString:htmlCode baseURL:NULL];
            }
            else{
                [[mapWebView mainFrame] loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString: @"about:blank"]]];
            }
        }
    }   
}

- (void) imageBrowserSelectionDidChange:(IKImageBrowserView *) aBrowser{
    
    NSInteger selImg = [[imageBrowser selectionIndexes] firstIndex];
    if (selImg >= 0){
        BOOL imageWasFound = NO;
        NSInteger _imageSel = -1;
        NSInteger _logSel = -1;        
        for (LPImage* _im in images){
            _imageSel++;            
            if ([[_im pinName] isEqualToString:@"pin_green"]){
                continue;
            }
            _logSel++;
            
            if (_imageSel == selImg){
                imageWasFound = YES;
                break;
            }
            

            
        }    
        
        // No selection to be made
        if (!imageWasFound){
            [logTableView deselectAll:nil];
        }
        else{
            if ([[LPLogFile logFileAsArray] count] > 0){
                [logTableView selectRowIndexes:[NSIndexSet indexSetWithIndex:_logSel] byExtendingSelection:NO];
            }
        }
        
        // Only open the drawer if there is GPS info to show OR, if not GPS, after the image has been processed
        //        NSInteger selImg = [[imageBrowser selectionIndexes] firstIndex];
        //        if ([mapDrawer state] == NSDrawerClosedState) {
        if (selImg != NSNotFound){
            if ([[images objectAtIndex:selImg] hadInitiallyGPS] || [[images objectAtIndex:selImg] hasBeenProcessed])
                [mapDrawer open];
        //        }
            else{
//                if (selImg != NSNotFound && !([[images objectAtIndex:selImg] hadInitiallyGPS] && [[images objectAtIndex:selImg] hasBeenProcessed]))
                    [mapDrawer close];
            }
        }
        
        [self loadMapWebView];
    }
    
    
}


// Implement some optional methods of the image browser  datasource protocol to allow for removing and reodering items.

// -------------------------------------------------------------------------
//	removeItemsAtIndexes:
//
//	The user wants to delete images, so remove these entries from the data source.	
// ------------------------------------------------------------------------- 
- (void)imageBrowser:(IKImageBrowserView*)view removeItemsAtIndexes: (NSIndexSet*)indexes
{
	[images removeObjectsAtIndexes:indexes];
}

// -------------------------------------------------------------------------
//	moveItemsAtIndexes:
//
//	The user wants to reorder images, update the datadsource and the browser
//	will reflect our changes.
// ------------------------------------------------------------------------- 
- (BOOL)imageBrowser:(IKImageBrowserView*)view moveItemsAtIndexes: (NSIndexSet*)indexes toIndex:(NSUInteger)destinationIndex
{
	NSInteger		index;
	NSMutableArray*	temporaryArray;
    
	temporaryArray = [[[NSMutableArray alloc] init] autorelease];
    
	// First remove items from the data source and keep them in a temporary array.
	for (index = [indexes lastIndex]; index != NSNotFound; index = [indexes indexLessThanIndex:index])
	{
		if (index < destinationIndex)
            destinationIndex --;
        
		id obj = [images objectAtIndex:index];
		[temporaryArray addObject:obj];
		[images removeObjectAtIndex:index];
	}
    
	// Then insert the removed items at the appropriate location.
	NSInteger n = [temporaryArray count];
	for (index = 0; index < n; index++)
	{
		[images insertObject:[temporaryArray objectAtIndex:index] atIndex:destinationIndex];
	}
    
	return YES;
}

- (void)selectAllImages{
    [imageBrowser setSelectionIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0,[images count])] byExtendingSelection:NO];
}

- (NSInteger) numberOfImages{
    return [images count];
}


- (BOOL)isProperFormatWithURL:(NSURL*)fileURL{
    NSArray* formats = (NSArray*)CGImageSourceCopyTypeIdentifiers();
    NSString*   itemUTI = nil;
    
    [fileURL getResourceValue:&itemUTI forKey:NSURLTypeIdentifierKey error:nil];
    return [formats containsObject:itemUTI];
}


#pragma mark -
#pragma mark Authentication Methods
// -----------------------------------------------------------------
// AUTHENTICATION METHODS
// -----------------------------------------------------------------

- (NSString *)signedInUsername {
    // Get the email address of the signed-in user
    GTLServiceLatitude *service = [myLatitude latitudeService];
    GTMOAuth2Authentication *auth = [service authorizer];
    BOOL isSignedIn = [auth canAuthorize];
    
//    [self updateUI];
    
    if (isSignedIn) {
        [addPhotosButton_ setEnabled:YES];        
        return [auth userEmail];        
    } else {    
        [addPhotosButton_ setEnabled:NO];
        [locationsCancelButton_ setEnabled:NO];
        [locationsCancelButton_ setTransparent:YES];        
        return nil;
    }
}

- (void)signInFetchStateChanged:(NSNotification *)note {
    // This just lets the user know something is happening during the
    // sign-in sequence's "invisible" fetches to obtain tokens
    //
    // The fetcher is available as
    //   [[note userInfo] objectForKey:kGTMOAuth2FetcherKey]
    //
    // The type of token obtained is available on the start notification as
    //   [[note userInfo] objectForKey:kGTMOAuth2FetchTypeKey]
    //
    if ([[note name] isEqual:kGTMOAuth2FetchStarted]) {
        mAuthFetchersRunningCount++;
    } else if (mAuthFetchersRunningCount > 0) {
        mAuthFetchersRunningCount--;
    }
    
    if (![self signedInUsername]){
        if (mAuthFetchersRunningCount > 0) {
            [mSpinner startAnimation:self];
        } else {
            [mSpinner stopAnimation:self];
        }
    }
}

- (void)signInNetworkLost:(NSNotification *)note {
    // The network dropped for 30 seconds
    //
    // We could alert the user and wait for notification that the network has
    // has returned, or just cancel the sign-in sheet, as shown here
    GTMOAuth2SignIn *signIn = [note object];
    GTMOAuth2WindowController *controller = [signIn delegate];
    [controller cancelSigningIn];
}




- (BOOL)isSignedIn {
    NSString *name = [self signedInUsername];
    return (name != nil);
}


- (void)runSigninThenInvokeSelector:(SEL)signInDoneSel {
    NSString *scope = [GTMOAuth2Authentication scopeWithStrings:
                       kGTLAuthScopeLatitudeAllBest,
                       kGTLAuthScopeLatitudeCurrentBest,
                       nil];
    
    
    NSBundle *frameworkBundle = [NSBundle bundleForClass:[GTMOAuth2WindowController class]];
    GTMOAuth2WindowController *windowController;
    windowController = [GTMOAuth2WindowController controllerWithScope:scope
                                                             clientID:_CLIENT_ID
                                                         clientSecret:_CLIENT_SECRET
                                                     keychainItemName:_KEYCHAIN_ITEM_NAME
                                                       resourceBundle:frameworkBundle];
    
    [windowController signInSheetModalForWindow:[self window]
                              completionHandler:^(GTMOAuth2Authentication *auth,
                                                  NSError *error) {
                                  // callback
                                  if (error == nil) {
                                      myLatitude.latitudeService.authorizer = auth;
                                      if (signInDoneSel) {
                                          [self performSelector:signInDoneSel];
                                      }
                                  } else {
                                      myLatitude.locationsFetchError = error;
                                      [self updateUI];
                                  }
                              }];
}

# pragma mark -
- (void)updateUI {
    BOOL isSignedIn = [self isSignedIn];
    NSString *username = [self signedInUsername];
    [signedInButton_ setTitle:(isSignedIn ? @"Sign Out" : @"Sign In")];
    [signedInField_ setStringValue:(isSignedIn ? username : @"")];
    
    
    if ([self.class isRunning]){
        [locationsProgressIndicator_ startAnimation:self];
        [locationsCancelButton_ setEnabled:YES];
        [locationsCancelButton_ setTransparent:NO];
        [addPhotosButton_ setEnabled:NO];     
        [startProcessButton_ setEnabled:NO];
        [clearAllButton_ setEnabled:NO];
        [clearAllButton_ setTransparent:YES];
    }
    else{
        [locationsProgressIndicator_ stopAnimation:self];
        [locationsCancelButton_ setEnabled:NO];
        [locationsCancelButton_ setTransparent:YES];
        [addPhotosButton_ setEnabled:YES];     
        [startProcessButton_ setEnabled:YES];
        [clearAllButton_ setEnabled:YES];
        [clearAllButton_ setTransparent:NO];        
    }
    
    
    if ([images count] > 0){
        [startProcessButton_ setEnabled:YES];
        [clearAllButton_ setEnabled:YES];
        [clearAllButton_ setTransparent:NO];        
        [startProcessButton_ setEnabled:YES];                
    }
    else{
        [startProcessButton_ setEnabled:NO];
        [clearAllButton_ setEnabled:NO];        
        [clearAllButton_ setTransparent:YES];        
        [startProcessButton_ setEnabled:NO];        
        [[mapWebView mainFrame] loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString: @"about:blank"]]];
        [mapDrawer close];
    }
    
    
    if ([images count] > 0 && ![self.class isRunning])
        [startProcessButton_ setEnabled:YES]; 
    else
        [startProcessButton_ setEnabled:NO]; 
    
    
    if (isSignedIn){
        [addPhotosButton_ setEnabled:YES];
    }
    else{
        [addPhotosButton_ setEnabled:NO];
    }
}



#pragma mark -
#pragma mark import images from file system

// -------------------------------------------------------------------------
//	isImageFile:filePath
//
//	This utility method indicates if the file located at 'filePath' is
//	an image file based on the UTI. It relies on the ImageIO framework for the
//	supported type identifiers.
//
// -------------------------------------------------------------------------
- (BOOL)isImageFile:(NSString*)filePath
{
	BOOL				isImageFile = NO;
	LSItemInfoRecord	info;
	CFStringRef			uti = NULL;
	
	CFURLRef url = CFURLCreateWithFileSystemPath(NULL, (CFStringRef)filePath, kCFURLPOSIXPathStyle, FALSE);
	
	if (LSCopyItemInfoForURL(url, kLSRequestExtension | kLSRequestTypeCreator, &info) == noErr)
	{
		// Obtain the UTI using the file information.
		
		// If there is a file extension, get the UTI.
		if (info.extension != NULL)
		{
			uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, info.extension, kUTTypeData);
			CFRelease(info.extension);
		}
        
		// No UTI yet
		if (uti == NULL)
		{
			// If there is an OSType, get the UTI.
			CFStringRef typeString = UTCreateStringForOSType(info.filetype);
			if ( typeString != NULL)
			{
				uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassOSType, typeString, kUTTypeData);
				CFRelease(typeString);
			}
		}
		
		// Verify that this is a file that the ImageIO framework supports.
		if (uti != NULL)
		{
			CFArrayRef  supportedTypes = CGImageSourceCopyTypeIdentifiers();
			CFIndex		i, typeCount = CFArrayGetCount(supportedTypes);
            
			for (i = 0; i < typeCount; i++)
			{
				if (UTTypeConformsTo(uti, (CFStringRef)CFArrayGetValueAtIndex(supportedTypes, i)))
				{
					isImageFile = YES;
					break;
				}
			}
		}
	}
	
	return isImageFile;
}

// -----------------------------------------------------------------
// PICTURES SELECTION
// -----------------------------------------------------------------


- (void) addAnImageWithURL:(NSURL *) url
{
    
    LPImage * image = [[LPImage alloc] initWithURL:url];
    if (!image){
        NSLog(@"ERROR PROCESSING %@. Was Skipped!", [url path]);
        [LPLogFile writeString:[NSString stringWithFormat:@"Error   : Could not initialize %@. Was Skipped!", [url lastPathComponent]]];
        return;
        
    }
    
    [importedImages addObject:image];
    [image release];
}

- (void) addImagesWithURL:(NSURL *) url recursive:(BOOL) recursive
{
    
    // Check if image already in UI, get out if it is
    for (LPImage *img in images){
        if ([[img URL] isEqual:(NSURL*)url])
            return;
    }
    
    if ([self isProperFormatWithURL:url]){
        int i, n;
        BOOL isDir;
        NSError *myErr;
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:[url path] isDirectory:&isDir] && isDir)
        {
            NSString *path = [url path];
            
            NSArray *content = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:&myErr];
            
            n = [content count];
            for(i=0; i<n; i++){
                if(recursive)
                    [self addImagesWithURL: [url URLByAppendingPathComponent: [content objectAtIndex:i]] recursive:YES];
                else
                    [self addAnImageWithURL: [url URLByAppendingPathComponent: [content objectAtIndex:i]]];
            }
        }
        else
            [self addAnImageWithURL:url];
        
        
        // Add to recent images
        [[NSDocumentController sharedDocumentController] noteNewRecentDocumentURL:url];
    }
}


- (IBAction)selectFileOrFolderClicked:(id)sender
{   
    
    
    NSOpenPanel *openPanelSheet = [NSOpenPanel openPanel];
    [openPanelSheet setCanChooseFiles:true];
    [openPanelSheet setAllowsMultipleSelection:YES];
    [openPanelSheet setCanChooseDirectories:YES];
    
    [openPanelSheet beginSheetModalForWindow:[self window] completionHandler:^(NSInteger result) {
        NSArray *files = nil;
        if (result == NSFileHandlingPanelOKButton){
            files = [openPanelSheet URLs];
            [NSThread detachNewThreadSelector:@selector(addImagesWithPaths:) toTarget:self withObject:files];
            
        }
        
        if ([files count] > 0){
            
            [startProcessButton_ setEnabled:YES];
            [clearAllButton_ setEnabled:YES];
            [clearAllButton_ setTransparent:NO];
        }
    }];
    
}


-(void) addImagesWithPaths:(NSMutableArray *) files{
    for (int i =0; i < [files count];i++)
    {       
            NSLog(@"%@",[NSString stringWithFormat:@"Adding %@", [files objectAtIndex:i]]);
            [self addImagesWithURL: [files objectAtIndex:i] recursive:YES];
            
            // Update the data source in the main thread.
            [self performSelectorOnMainThread:@selector(updateDatasource) withObject:nil waitUntilDone:YES];
    }
}

#pragma mark -
#pragma mark drag n drop 

// -------------------------------------------------------------------------
//	draggingEntered:sender
// ------------------------------------------------------------------------- 
- (NSDragOperation)draggingEntered:(id <NSDraggingInfo>)sender
{
    return NSDragOperationCopy;
}

// -------------------------------------------------------------------------
//	draggingUpdated:sender
// ------------------------------------------------------------------------- 
- (NSDragOperation)draggingUpdated:(id <NSDraggingInfo>)sender
{
    return NSDragOperationCopy;
}

// -------------------------------------------------------------------------
//	performDragOperation:sender
// ------------------------------------------------------------------------- 
- (BOOL)performDragOperation:(id <NSDraggingInfo>)sender
{
    NSData*			data = nil;
    NSPasteboard*	pasteboard = [sender draggingPasteboard];
    
	// Look for paths on the pasteboard.
    if ([[pasteboard types] containsObject:NSFilenamesPboardType]) 
        data = [pasteboard dataForType:NSFilenamesPboardType];
    
    if (data)
	{
		NSString* errorDescription;
		
		// Retrieve  paths.
        NSArray* paths = [NSPropertyListSerialization propertyListFromData:data 
                                                          mutabilityOption:kCFPropertyListImmutable 
                                                                    format:nil 
                                                          errorDescription:&errorDescription];
        
        
        // Turn array of paths (NSString) into aray of NSURLs
        
        for (NSString * path in paths){
            [self addImagesWithURL: [NSURL fileURLWithPath:path] recursive:YES];
        }
		
		// Make the image browser reload the data source.
        [self updateDatasource];
        
        [self updateUI];
    }
    
	// Accept the drag operation.
	return YES;
}

#pragma mark -
#pragma mark IB Controls
// -----------------------------------------------------------------
// IB - CLICKED CONTROLS
// -----------------------------------------------------------------

- (IBAction)openCloseLogDrawer:(id)sender{
    if ([logDrawer state] == NSDrawerClosedState){
        [logDrawer open];
        
    }
    else
        [logDrawer close];
    
}

// -------------------------------------------------------------------------
//	addImageButtonClicked:sender
//
//	Action called when the zoom slider changes.
// ------------------------------------------------------------------------- 
- (IBAction)zoomSliderDidChange:(id)sender
{
	// update the zoom value to scale images
    [imageBrowser setZoomValue:[sender floatValue]];
	
	// redisplay
    [imageBrowser setNeedsDisplay:YES];
}

- (IBAction)signInClicked:(id)sender {
    if (![self isSignedIn]) {
        // sign in
        [self runSigninThenInvokeSelector:@selector(updateUI)];
    } else {
        // sign out
        GTLServiceLatitude *service = [myLatitude latitudeService]; 
        
        [GTMOAuth2WindowController removeAuthFromKeychainForName:kKeychainItemName];
        [service setAuthorizer:nil];
        [self clearAll];
    }
    
    [self updateUI];
}

+(BOOL) isRunning{
    return isRunning_;
}

+(void) setIsRunning:(BOOL) in_isRunning{
    isRunning_ = in_isRunning;
}


- (void) processTagAction{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    [LPLogFile createNewLogFile];
    NSLog(@"%@", [NSString stringWithFormat:@"         Starting tagging operation for %d images at %@", [images count], 
                  [dateFormatter stringFromDate:[NSDate date]]]);

    
    [self.class setIsRunning:YES];
    [self updateUI];
    
    [myLatitude setImageswithImages:images];
    
    // Create timer
    timer = [NSTimer scheduledTimerWithTimeInterval: 0.1
                                             target: self
                                           selector: @selector(onTick:)
                                           userInfo: nil
                                            repeats: YES];
    
    
    
}

- (void) updateDrawerUI{
    [logTableView reloadData];
    
    // Scroll to bottom
    NSPoint newScrollOrigin;
    
    if ([[logScrollView documentView] isFlipped]) {
        newScrollOrigin=NSMakePoint(0.0,NSMaxY([[logScrollView documentView] frame])
                                    -NSHeight([[logScrollView contentView] bounds]));
    } else {
        newScrollOrigin=NSMakePoint(0.0,0.0);
    }
    
    [[logScrollView documentView] scrollPoint:newScrollOrigin];
    
    
    // Set Log Drawer size
    NSSize mySize;
    mySize.height = ([[LPLogFile logFileAsArray] count] * 23) + 22;  // fixed row height
    [logDrawer setContentSize:mySize];
    
}

-(void)onTick:(NSTimer *)timer {
    
    for (LPImage* im in images){
        
        // Are all images in array processed?
        if ([LatitudeController isRunning]){
            if (![im hasBeenProcessed]){
                [self updateDrawerUI];
                return;
            }
        }
    }
    
    // Resort images by returning rank
    
    [images sortUsingComparator:^NSComparisonResult(LPImage *im1, LPImage *im2) {
        if (im1.returningRank < im2.returningRank)
            return (NSComparisonResult)NSOrderedAscending;
        if (im1.returningRank > im2.returningRank)
            return (NSComparisonResult)NSOrderedDescending;
        return (NSComparisonResult)NSOrderedSame;
        
    }];
    
    // Re-add tghe pictures to pick up the new pin color
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    [tempArray addObjectsFromArray:images];
    [images removeAllObjects];

    [imageBrowser reloadData];
    [images addObjectsFromArray:tempArray];
    [imageBrowser reloadData];
    
    [self updateDatasource];  
    
    
    [timer invalidate];
    timer = nil;
    [self.class setIsRunning:NO];
    [self updateUI];
    
    [self updateDrawerUI];
}

- (void) clearAll{
    [images removeAllObjects];
    [importedImages removeAllObjects];
    [imageBrowser reloadData];
    
    [LPLogFile setNoLogFile];
    [self updateDrawerUI];
    [logDrawer close];
    
    [self updateUI];
}

- (IBAction)clearAllClicked:(id)sender{
    [self clearAll];
}

- (IBAction)getLocationsClicked:(id)sender {
    [self processTagAction];
    
}

- (IBAction)cancelLocationsFetchClicked:(id)sender {
    [self.class setIsRunning:NO];
    
    // cancel any location feed fetch
    [[myLatitude locationsTicket] cancelTicket];
    [myLatitude setLocationsTicket:nil];
    
    
    [self updateUI];
}

- (BOOL) isAddPhotosButtonEnabled{
    return [addPhotosButton_ isEnabled];
}

# pragma mark -
# pragma mark Log Drawer

- (NSInteger)numberOfRowsInTableView:(NSTableView *)aTableView
{
    NSInteger logCount = [[LPLogFile logFileAsArray] count];
    if (logCount > 0){
        if ([logDrawer state] == NSDrawerClosedState)
            [logDrawer open];   
        
        [logButton_ setTransparent:NO];
        [logButton_ setEnabled:YES];        
    }
    else{
        [logButton_ setTransparent:YES];
        [logButton_ setEnabled:NO];                
    }
    
    return logCount;
}

- (id)tableView:(NSTableView *)aTableView objectValueForTableColumn:(NSTableColumn *)atableColumn 
            row:(NSInteger)rowIndex
{
    NSInteger selRow = [logTableView selectedRow];
    
    if (selRow >= 0){
        NSInteger _imageSel = -1;
        NSInteger _logSel = 0;        
        for (LPImage* _im in images){
            _imageSel++;            
            if ([[_im pinName] isEqualToString:@"pin_green"]){
                continue;
            }
            
            if (_logSel == selRow)
                break;

            _logSel++;

        }
        
        [imageBrowser setSelectionIndexes:[NSIndexSet indexSetWithIndex:_imageSel]
                     byExtendingSelection:NO];
    }
    
    return [[LPLogFile logFileAsArray] objectAtIndex:rowIndex];
}

- (void)tableView:(NSTableView *)tableView willDisplayCell:(id)aCell forTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {

    NSTextFieldCell *cell = aCell;
    [cell setDrawsBackground:YES];
    if ([tableView selectedRow] == row){
        NSString* text = [[LPLogFile logFileAsArray] objectAtIndex:row];
        if ([text rangeOfString:@"Warning"].location != NSNotFound)
            [cell setBackgroundColor:[NSColor lightGrayColor]];            
        else
            [cell setBackgroundColor:[NSColor blackColor]];
        [cell setTextColor:[NSColor whiteColor]];        
    }
    else{
        [cell setBackgroundColor:[NSColor whiteColor]];
        [cell setTextColor:[NSColor blackColor]];                
    }

}

-(id)_highlightColorForCell:(NSCell *)cell{
    NSLog(@"really??");
    return [NSColor redColor];
}

# pragma mark Map Drawer

- (void) repaintMap{
    // This is so that http requests don't fire like a machine gun when user is resizing
    if (mapTimer != nil){
        [mapTimer invalidate];
        mapTimer = nil;
    }
    
    // Create timer
    mapTimer = [NSTimer scheduledTimerWithTimeInterval: 0.1
                                                target: self
                                              selector: @selector(onResizingUI:)
                                              userInfo: nil
                                               repeats: NO];
}

-(void)onResizingUI:(NSTimer *)timer {
    [self loadMapWebView];
    mapTimer = nil;
}

- (NSSize)drawerWillResizeContents:(NSDrawer *)sender toSize:(NSSize)contentSize{
    if ([sender isEqual:mapDrawer]){
        [self repaintMap];
    }
    
    return contentSize;
}

- (void)windowDidResize:(NSNotification *)notification{
    if ([mapDrawer state] == NSDrawerOpenState){
        [self repaintMap];
    }
}
@end
