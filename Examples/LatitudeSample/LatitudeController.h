/* Copyright (c) 2011 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//
//  LatitudeSampleWindowController.h
//

#import <Cocoa/Cocoa.h>
#import <CoreLocation/CoreLocation.h>
#import <Quartz/Quartz.h>
#import <WebKit/WebKit.h>
#import "GTLLatitude.h"
#import <ApplicationServices/ApplicationServices.h>  // For Image IO
#import "LPLogFile.h"

@interface LatitudeController : NSWindowController {

    IBOutlet NSTextField *signedInField_;
    IBOutlet NSButton *signedInButton_;
    
    IBOutlet NSProgressIndicator *mSpinner;
    IBOutlet NSProgressIndicator *locationsProgressIndicator_;
    IBOutlet NSButton *locationsCancelButton_;
    IBOutlet NSWindow *_openPanelWindow;
    IBOutlet NSButton *startProcessButton_;
    IBOutlet NSButton *addPhotosButton_; 
    IBOutlet NSButton *clearAllButton_; 
    
    NSUInteger mAuthFetchersRunningCount;
    
    NSMutableArray * images;
    NSMutableArray * importedImages;
    IBOutlet IKImageBrowserView* imageBrowser;    
    
    // Drawer
    IBOutlet NSTableView *logTableView;
    IBOutlet NSScrollView *logScrollView;
    IBOutlet NSTableColumn *logColumn;
    IBOutlet NSButton *openCloseLogDrawerButton_;
    IBOutlet NSDrawer *logDrawer;
    IBOutlet NSButton *logButton_;
    
    IBOutlet NSDrawer *mapDrawer;
    IBOutlet WebView *mapWebView;

}

+ (LatitudeController *)sharedWindowController;

- (IBAction)zoomSliderDidChange:(id)sender;
- (IBAction)signInClicked:(id)sender;
- (IBAction)getLocationsClicked:(id)sender;
- (IBAction)cancelLocationsFetchClicked:(id)sender;
- (IBAction)selectFileOrFolderClicked:(id)sender;
- (IBAction)openCloseLogDrawer:(id)sender;
- (IBAction)clearAllClicked:(id)sender;
- (BOOL) isAddPhotosButtonEnabled;

+(BOOL) isRunning;
+(void) setIsRunning:(BOOL) isRunning_;
-(void) addImagesWithPaths:(NSMutableArray *) files;
- (void)updateUI;
- (NSInteger) numberOfImages;
- (void) clearAll;
- (void)selectAllImages;
@end
