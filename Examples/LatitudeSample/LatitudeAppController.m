/* Copyright (c) 2011 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//
//  LatitudeSampleAppController.m
//

#import "LatitudeAppController.h"
#import "LatitudeController.h"

@implementation LatitudeAppController 
LatitudeController* windowController;

- (void)applicationWillFinishLaunching:(NSNotification *)notification {
    
    
    windowController = [LatitudeController sharedWindowController];
    [windowController showWindow:self];
    
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)sender
{
    return YES;
}


- (IBAction) showPrefsPane:(id)sender{
    LPPrefsPane* prefsPane = [LPPrefsPane sharedWindowController];
    [prefsPane showWindow:self];
}

- (IBAction) performOpenAction:(id)sender{
    [windowController selectFileOrFolderClicked:nil];  
}


- (BOOL)validateMenuItem:(NSMenuItem *)item {
    if ([item action] == @selector(performOpenAction:))
        return [windowController isAddPhotosButtonEnabled];
    
    if ([item action] == @selector(paste:)){
        NSPasteboard *pasteboard = [NSPasteboard generalPasteboard];
        NSArray *classArray = [NSArray arrayWithObject:[NSURL class]];
        NSDictionary *options = [NSDictionary dictionary];
        
        return [pasteboard canReadObjectForClasses:classArray options:options];
    }
    
    if ([item action] == @selector(selectAll:) && [windowController numberOfImages] == 0 ){
        return NO;
    }
    
    if ([item action] == @selector(clearAll:) && [windowController numberOfImages] == 0 ){
        return NO;
    }
    
    
    return YES;
}

- (IBAction)paste:sender {
    
    NSPasteboard *pasteboard = [NSPasteboard generalPasteboard];
    NSArray *classArray = [NSArray arrayWithObject:[NSURL class]];
    NSDictionary *options = [NSDictionary dictionary];
    
    BOOL ok = [pasteboard canReadObjectForClasses:classArray options:options];
    if (ok) {
        NSArray *objectsToPaste = [pasteboard readObjectsForClasses:classArray options:options];
        [NSThread detachNewThreadSelector:@selector(addImagesWithPaths:) toTarget:self withObject:objectsToPaste];   
    }
}

-(void) addImagesWithPaths:(NSMutableArray *) files{
    [windowController addImagesWithPaths:files];
    [windowController updateUI];
}

- (IBAction) selectAll:(id)sender{
    [windowController selectAllImages];
}

- (IBAction) clearAll:(id)sender{
    [windowController clearAll];
}

- (IBAction)displayHelp:(id)sender{
    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"http://www.blazingfrog.com/bf/LatipicsHelp.html"]];
}

- (IBAction) goToLatitudeHistory:(id)sender{
    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"https://www.google.com/latitude/b/0/history/manage"]];
}


- (BOOL) application:(NSApplication *)sender openFiles:(NSArray *)filenames{
    NSMutableArray *urls = [[NSMutableArray alloc] init];
    
    for (NSString* path in filenames){
        [urls addObject:[NSURL fileURLWithPath:path]];
    }
    
    [windowController addImagesWithPaths:urls];
    [windowController updateUI];    
    return YES;
}
@end
