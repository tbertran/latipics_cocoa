//
//  LPLogFile.m
//  Latipics
//
//  Created by Thomas Bertran on 11/1/11.
//  Copyright (c) 2011 self. All rights reserved.
//

#import "LPLogFile.h"

@implementation LPLogFile
static NSURL* URL_;

+ (NSString *) logDirPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);
    return [[[paths objectAtIndex:0] stringByAppendingString:@"/Latipics"] retain];
}

+ (void) createNewLogFile{
    
    BOOL isDir;
    // Create directory if not existent
    if (([[NSFileManager defaultManager] fileExistsAtPath:[self.class logDirPath] isDirectory:&isDir] && isDir) == FALSE)
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:[self.class logDirPath] withIntermediateDirectories:TRUE attributes: nil error:NULL];
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd_HH-mm-ss"];
    NSString * fileName = [NSString stringWithFormat:@"Latipics_%@.txt", [dateFormatter stringFromDate:[NSDate date]]];
    
    NSString* _URL = [[self.class logDirPath] stringByAppendingPathComponent:fileName];
    [URL_ release];
    URL_ = [[NSURL fileURLWithPath: _URL] retain];
}

+ (BOOL) writeString:(NSString *) _string{
    NSError *error;
    BOOL ok;
    
    NSString *tempString = [self.class fileContents];
    if (tempString){
        tempString = [NSString stringWithFormat:@"%@\n", tempString];
        ok = [[tempString stringByAppendingString:_string] writeToURL:URL_ atomically:NO encoding:NSUnicodeStringEncoding error:&error];
    }
    else
        ok = [_string writeToURL:URL_ atomically:NO encoding:NSUnicodeStringEncoding error:&error];
    
    // an error occurred
    if (!ok)
        NSLog(@"Error writing file at %@\n%@", [URL_ path], [error localizedFailureReason]);
    
    return ok;
}

+ (NSString *) fileContents{
    
    if (!URL_)
        return nil;
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:[URL_ path]]){
        return nil;
    }
              
    NSError *error;
    NSString *stringFromFile = [[NSString alloc]
                                      initWithContentsOfFile:[URL_ path]
                                      encoding:NSUnicodeStringEncoding
                                      error:&error];
    
    if (stringFromFile == nil) {
        // an error occurred
        NSLog(@"Error reading file at %@\n%@", [URL_ path], [error localizedFailureReason]);
        return nil;
    }
    
    return stringFromFile;
}

+ (NSArray *) logFileAsArray{
    return [[self.class fileContents] componentsSeparatedByString: @"\n"];
}

+ (void) setNoLogFile{
    URL_ = nil;
}
@end
