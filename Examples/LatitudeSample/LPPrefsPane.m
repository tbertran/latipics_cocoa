//
//  LPPrefsPane.m
//  Latipics
//
//  Created by Thomas Bertran on 10/28/11.
//  Copyright (c) 2011 self. All rights reserved.
//

#import "LPPrefsPane.h"

@implementation LPPrefsPane


- (id)init {    
    return [self initWithWindowNibName:@"PreferencePane"];
}


- (void)awakeFromNib {
    
    NSInteger latRangeFrom = [self.class latitudeRangeFrom];
    if (latRangeFrom){
        [fromTF setIntValue:latRangeFrom];
        [fromS setIntValue:60 - latRangeFrom];
    }
    else{
        [fromTF setIntValue:latRangeFrom];
        [fromS setIntValue:60 - latRangeFrom];
    }
    
    [toS setIntValue:[self.class latitudeRangeTo]];
    [toTF setIntValue:[self.class latitudeRangeTo]];
    
    [ovrdGPSCB setState:[self.class overrideGPSInfo]];
    [ovrdUpdDateCB setState:[self.class overrideUpdateDate]];    
    [keepCopyOriginalCB setState:[self.class keepCopyOriginal]];
}


/////////////
// Getters //
/////////////
# pragma mark -
# pragma mark Field Getters
+ (NSInteger) latitudeRangeFrom{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if ([defaults valueForKey:@"LatitudeRangeFrom"] == nil){
        [self.class setLatitudeRangeFrom:15];
        return 15;
    }
    else
        return [[defaults stringForKey:@"LatitudeRangeFrom"] integerValue];    
}

+ (NSInteger) latitudeRangeTo{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults valueForKey:@"LatitudeRangeTo"] == nil){
        [self.class setLatitudeRangeTo:15];
        return 15;
    }
    else
        return [[defaults stringForKey:@"LatitudeRangeTo"] integerValue];
}

+ (NSInteger) overrideGPSInfo{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults valueForKey:@"OverrideGPSInfo"] == nil){
        [self.class setOverrideGPSInfo:NSOffState];
        return NSOffState;
    }
    else{
        NSInteger state = [[defaults stringForKey:@"OverrideGPSInfo"] integerValue];
        return state;
    }
}

+ (NSInteger) overrideUpdateDate{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults valueForKey:@"OverrideUpdateDate"] == nil){
        [self.class setOverrideUpdateDate:NSOnState];
        return NSOnState;
    }
    else{
        NSInteger state = [[defaults stringForKey:@"OverrideUpdateDate"] integerValue];
        return state;
    }
}

+ (NSInteger) keepCopyOriginal{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults valueForKey:@"KeepCopyOfOriginal"] == nil){
        [self.class setKeepCopyOriginal:NSOnState];
        return NSOnState;
    }
    else{
        NSInteger state = [[defaults stringForKey:@"KeepCopyOfOriginal"] integerValue];
        return state;
    }
}

/////////////
// Setters //
/////////////
# pragma mark -
# pragma mark Field Setters
+ (void) setLatitudeRangeFrom:(NSInteger) from{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:from forKey:@"LatitudeRangeFrom"];
    [defaults synchronize];
}
+ (void) setLatitudeRangeTo:(NSInteger) to{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:to forKey:@"LatitudeRangeTo"];
    [defaults synchronize];
}
+ (void) setOverrideGPSInfo:(BOOL) doOvrdGPS{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:doOvrdGPS forKey:@"OverrideGPSInfo"];   
    [defaults synchronize];
}
+ (void) setOverrideUpdateDate:(BOOL) doOvrdUpdtDate{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:doOvrdUpdtDate forKey:@"OverrideUpdateDate"];  
    [defaults synchronize];
}
+ (void) setKeepCopyOriginal:(BOOL) doKeepCopyOriginal{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:doKeepCopyOriginal forKey:@"KeepCopyOfOriginal"];  
    [defaults synchronize];
}

////////
// IB //
////////
# pragma mark -
# pragma mark Interface Builder Stuff
- (IBAction) fromTfEntering:(id) sender{
    NSInteger val = 60 - [sender intValue];
    if (val > 60){
        val = 60;
        [fromTF setStringValue:@"0"];
    }
    if (val < 0){
        val = 0; 
        [fromTF setStringValue:@"60"];
    }

    [fromS setIntValue:val];
    [self.class setLatitudeRangeFrom:val];
}

- (IBAction) toTfEntering:(id) sender{
    NSInteger val = [sender intValue];
    if (val > 60)
        val = 60;
    if (val < 0)
        val = 0;
    
    [toTF setStringValue:[NSString stringWithFormat:@"%d",val]];
    [toS setIntValue:val];
    [self.class setLatitudeRangeTo:val];
}

- (IBAction) fromSliding:(id) sender{
    NSInteger val = 60 - [sender intValue];
    [fromTF setStringValue:[NSString stringWithFormat:@"%d",val]];
    [self.class setLatitudeRangeFrom:val];
}

- (IBAction) toSliding:(id) sender{
    
    [toTF setStringValue:[NSString stringWithFormat:@"%d",[sender intValue]]];
    [self.class setLatitudeRangeTo:[sender intValue]];
    
}

- (IBAction) ovrdGPSCheck:(id) sender{
    NSInteger state = [sender integerValue];
    [self.class setOverrideGPSInfo:state];
}

- (IBAction) ovrdUpdDateCheck:(id) sender{
    NSInteger state = [sender integerValue];
    [self.class setOverrideUpdateDate:state];
}

- (IBAction) keepCopyOfOriginalCheck:(id) sender{
    NSInteger state = [sender integerValue];
    [self.class setKeepCopyOriginal:state];
}


+ (LPPrefsPane *)sharedWindowController {
    static LPPrefsPane* gLPPrefsPane = nil;
    if (!gLPPrefsPane) {
        gLPPrefsPane = [[LPPrefsPane alloc] init];
    }

    return gLPPrefsPane;
}

@end