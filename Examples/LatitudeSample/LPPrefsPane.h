//
//  LPPrefsPane.h
//  Latipics
//
//  Created by Thomas Bertran on 10/28/11.
//  Copyright (c) 2011 self. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface LPPrefsPane : NSWindowController{

    IBOutlet NSTextField *fromTF;
    IBOutlet NSTextField *toTF;
    IBOutlet NSSlider *fromS;
    IBOutlet NSSlider *toS;
    IBOutlet NSButton *ovrdGPSCB;    
    IBOutlet NSButton *ovrdUpdDateCB;
    IBOutlet NSButton *keepCopyOriginalCB;    
}

+ (LPPrefsPane *)sharedWindowController;

- (IBAction) fromSliding:(id) sender;
- (IBAction) toSliding:(id) sender;
- (IBAction) ovrdGPSCheck:(id) sender;
- (IBAction) ovrdUpdDateCheck:(id) sender;
- (IBAction) keepCopyOfOriginalCheck:(id) sender;
- (IBAction) fromTfEntering:(id) sender;
- (IBAction) toTfEntering:(id) sender;

+ (NSInteger) latitudeRangeFrom;
+ (NSInteger) latitudeRangeTo;
+ (NSInteger) overrideGPSInfo;
+ (NSInteger) overrideUpdateDate;
+ (NSInteger) keepCopyOriginal;

+ (void) setLatitudeRangeFrom:(NSInteger) from;
+ (void) setLatitudeRangeTo:(NSInteger) to;
+ (void) setOverrideGPSInfo:(BOOL) doOvrdGPS;
+ (void) setOverrideUpdateDate:(BOOL) doOvrdUpdtDate;
+ (void) setKeepCopyOriginal:(BOOL) doKeepCopyOriginal;

@end
