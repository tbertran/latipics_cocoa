//
//  LPLatitude.m
//  Latipics
//
//  Created by Thomas Bertran on 10/3/11.
//  Copyright 2011 self. All rights reserved.
//

#import "LPLatitude.h"
#import "LPImage.h"

@implementation LPLatitude
static GTLServiceLatitude* service;

@synthesize locationsFeed = locationsFeed_,
locationsTicket = locationsTicket_,
locationsFetchError = locationsFetchError_;

NSInteger remainingImagesIdx;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

// get a service object with the current username/password
//
// A "service" object handles networking tasks.  Service objects
// contain user authentication information as well as networking
// state information (such as cookies and the "last modified" date for
// fetched data.)

- (GTLServiceLatitude *)latitudeService {
    
    //     service = nil;
    //    
    if (!service) {
        service = [[GTLServiceLatitude alloc] init];
        
        // have the service object fetch consecutive pages of the feed so we
        // do not need to manually fetch them
        service.shouldFetchNextPages = YES;
        
        // have the service object retry temporary error conditions automatically
        service.retryEnabled = YES;
    }
    return service;
}

- (void)dealloc {
    [locationsFeed_ release];
    [locationsTicket_ release];
    [locationsFetchError_ release];
    
    [super dealloc];
}


# pragma mark Utilities
// -----------------------------------------------------------------
// UTILITIES
// -----------------------------------------------------------------
+ (NSString *)timestampForDate:(NSDate *)date {
    NSTimeInterval seconds = [date timeIntervalSince1970];
    double millisecs = seconds * 1000.0;
    unsigned long long val = (long long)millisecs;
    NSString *str = [NSString stringWithFormat:@"%qu", val];
    return str;
    
}

+ (NSDate *)dateForTimestamp:(NSString *)timestamp {
    // the server returns timestamp as a string, milliseconds since the unix epoch
    long long val = [timestamp longLongValue];
    NSTimeInterval seconds = (NSTimeInterval)val / 1000.0;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:seconds];
    return date;
}

#pragma mark Fetch the locations feed

// begin retrieving the feed
- (void)processLocationWithImage:(LPImage *)image{
    
    self.locationsFeed = nil;
    self.locationsFetchError = nil;
    
    GTLQueryLatitude *query;
    query = [GTLQueryLatitude queryForLocationList];
    
    [query setGranularity:@"best"];
    
    NSTimeInterval periodBefore = [LPPrefsPane latitudeRangeFrom] * -60; 
    NSTimeInterval periodAfter = [LPPrefsPane latitudeRangeTo] * 60; 
    
    NSDate * minDate = [[image imageDate] dateByAddingTimeInterval:periodBefore];
    NSDate * maxDate = [[image imageDate] dateByAddingTimeInterval:periodAfter];
    
    NSLog(@"...searching GLatitude for date range %@ to %@", minDate, maxDate);
    [query setMinTime:[[self class] timestampForDate:minDate]];
    [query setMaxTime:[[self class] timestampForDate:maxDate]];
    
    

    GTLServiceLatitude *service = self.latitudeService;
    
    self.locationsTicket = [service executeQuery:query
                               completionHandler:^(GTLServiceTicket *ticket,
                                                   id object, NSError *error) {
                                   
                                   
                                   // Get out if user cancelled
                                   if (![LatitudeController isRunning])
                                       return;
                                   
                                   // callback
                                   GTLLatitudeLocationFeed *feed;
                                   
                                   if ([object isKindOfClass:[GTLLatitudeLocation class]]) {
                                       // we fetched the current location, which is an item rather than a feed;
                                       // we'll create a feed around the item
                                       feed = [[[GTLLatitudeLocationFeed alloc] init] autorelease];
                                       feed.items = [NSMutableArray arrayWithObject:object];
                                       
                                   } else {
                                       // we fetched a feed
                                       feed = (GTLLatitudeLocationFeed *)object;
                                   }
                                   
                                   self.locationsFeed = feed;
                                   self.locationsFetchError = error;
                                   self.locationsTicket = nil;
                                   
                                   NSArray *items = self.locationsFeed.items;
                                   
                                   if (error == nil && [items count] > 0){
                                       
                                       GTLLatitudeLocation *location = nil;
                                       
                                       NSInteger tempDiff = NSIntegerMax;
                                       // Find the location the closest (by timestamp) to the picture
                                       for (GTLLatitudeLocation *item in items){
                                           NSInteger imageTS = [[[self class] timestampForDate:[image imageDate]] integerValue];
                                           NSInteger itemTS = [[item timestampMs] longLongValue]; 
                                           NSInteger diff = fabs(imageTS - itemTS);
                                           
                                           if (diff < tempDiff){
                                               location = item;
                                               tempDiff = diff;
                                           }
                                       }
                                       
                                       [image setLatitude:[[location latitude] doubleValue]];
                                       [image setLongitude:[[location longitude] doubleValue]];
                                       [image setGLtimestampMs:[[location timestampMs] longLongValue]];                                       
                                       
                                       NSLog(@"Location found for %@: %@ - %@ @ %@",[image path], [location latitude], [location longitude], [location timestampMs]);
                                       [image setHasErrors:NO];                                       
                                       // *** UPDATE IMAGE ****
                                       [image saveImage];
                                            
                                   }
                                   else{
                                       NSLog(@"Error  : No Latitude data found for %@. Skipped. %@", [image path], error);
                                       [LPLogFile writeString:[NSString stringWithFormat:@"Error   : No Latitude data found for %@. It could be a network error, please retry.", [[image URL] lastPathComponent]]];
                                       [image setHasErrors:YES];
                                   }

                                   // Regardless of whether it was successful
                                   [image setHasBeenProcessed:YES];
                                   [image setReturningRank: ++remainingImagesIdx];

                               }];
    
}

         
- (void) setImageswithImages:(NSMutableArray *)in_images {
    images = in_images;
    
    remainingImagesIdx = -1;

    for (LPImage *im in images){
        
        // Reset all the hasBeenProcessed on all images
        [im setHasBeenProcessed:NO];
        
        if (![LatitudeController isRunning])
            return;

        [self processLocationWithImage:im];
        
    }
}



@end
